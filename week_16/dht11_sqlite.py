mport time
import sqlite3
import board
import adafruit_dht

dhtDevice = adafruit_dht.DHT11(board.D17)

def read_sensor():
        try:
                temperature_c = dhtDevice.temperature
                humidity = dhtDevice.humidity
                print("Temp: {:.1f} C    Humidity: {}% "
                        .format(temperature_c, humidity))
        except RuntimeError as error:
                print(error.args[0])
        time.sleep(2.0)
        return temperature_c, humidity


conn = sqlite3.connect(r'test.db')
c = conn.cursor()

drop = '''DROP TABLE IF EXISTS MEASUREMENTS'''
c.execute(drop)

sql = '''CREATE TABLE MEASUREMENTS(
    DEVICE_ID INTEGER PRIMARY KEY,
    TEMPERATURE FLOAT NOT NULL,
    HUMIDITY FLOAT NOT NULL
)'''
c.execute(sql)
for i in range(10):
        insert = '''INSERT INTO MEASUREMENTS(TEMPERATURE, HUMIDITY)
                VALUES (?,?)'''
        c.execute(insert, read_sensor())

conn.commit()