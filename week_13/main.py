from dht import DHT11
from button import ButtonLED, OffState
from machine import Pin, PWM, ADC

#Button1 GPIO16
#LED1 GPIO18
#Button2 GPIO4
#LED2 GPIO2
#Button3 GPIO25
#LED3 GPIO17
#DHT11 GPIO14
#LDR GPIO32

frequency = 5000
button1 = Pin(16, Pin.IN)
led1 = PWM(Pin(18, Pin.OUT), frequency)
button2 = Pin(4, Pin.IN)
led2 = PWM(Pin(2, Pin.OUT), frequency)
button3 = Pin(25, Pin.IN)
led3 = PWM(Pin(17, Pin.OUT), frequency)
ldr = ADC(Pin(32))
dht = DHT11(Pin(14))

button_led1 = ButtonLED(OffState(led1), 0, ldr, dht)
button_led2 = ButtonLED(OffState(led2), 0, ldr, dht)
button_led3 = ButtonLED(OffState(led3), 0, ldr, dht)
led1.duty(0)
led2.duty(0)
led3.duty(0)

while True:
    if button1.value() == True:
        button_led1.execute()
    elif button2.value() == True:
        button_led2.execute()
    elif button3.value() == True:
        button_led3.execute()