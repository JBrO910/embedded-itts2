from dht import DHT11
from machine import Pin, PWM, ADC
from time import sleep
import time

class ButtonLED:
    _state = None
    _ldr = None
    _dht = None
    _counter = None
    def __init__(self, state, counter, ldr, dht) -> None:
        self._counter = counter
        self._ldr = ADC(Pin(32))
        self._dht = dht
        self._state = state

    def changeState(self, state: ButtonState):
        self._state = state

    def execute(self):
        if self._counter == 0:
            self.changeState(OnState(self._state.ledPin))
            print("ON")
            self._counter += 1
        elif self._counter == 1:
            self.changeState(PulsatingState(self._state.ledPin))
            print("PULSATING")
            self._counter += 1
        elif self._counter == 2:
            self.changeState(OffState(self._state.ledPin))
            print("OFF")
            self._counter += 1
        elif self._counter == 3:
            self.changeState(DayNight(self._state.ledPin, self._ldr))
            print("LDR DAY/NIGHT")
            self._counter += 1
        elif self._counter == 4:
            self.changeState(LDR(self._state.ledPin, self._ldr))
            print("LDR")
            self._counter += 1    
        elif self._counter == 5:
            self.changeState(Temp(self._state.ledPin, self._dht))
            print("TEMP")
            self._counter = 0
        self._state.behavior()
        sleep(1)
        print("DONE SWITCHING STATE")
            
class ButtonState:
    def __init__(self, ledPin):
        self.ledPin = ledPin
        
    def behavior(self) -> None:
        pass

class OffState(ButtonState):
    def behavior(self) -> None:
        self.ledPin.duty(0)

class OnState(ButtonState):
    def behavior(self) -> None:
        self.ledPin.duty(1023)

class PulsatingState(ButtonState):  
    def behavior(self) -> None:
        for duty_cycle in reversed(range(0, 1024)):
            self.ledPin.duty(duty_cycle)
            sleep(0.001)
        for duty_cycle in range(0, 1024):
            self.ledPin.duty(duty_cycle)
            sleep(0.001)

class TempLDRState(ButtonState):
    def __init__(self, ledPin, sensorPin):
        self.ledPin = ledPin
        self.sensorPin = sensorPin

    def readLDR(self):
        return int((1023 - 0) * self.sensorPin.read() / 4095)
    
    def readTemp(self):
        self.sensorPin.measure()
        return int((1023 - 0) * self.sensorPin.temperature() / 50)

class DayNight(TempLDRState):
    def behavior(self) -> None:
        if self.readLDR() < 1023/2:
            self.ledPin.duty(1023)
        else:
            self.ledPin.duty(0)

class LDR(TempLDRState):
    def behavior(self) -> None:
        t_end = time.time() + 5
        while time.time() < t_end:
            self.ledPin.duty(self.readLDR())

class Temp(TempLDRState):
    def behavior(self) -> None:
        t_end = time.time() + 5
        while time.time() < t_end:
            self.ledPin.duty(self.readTemp())
            sleep(1)